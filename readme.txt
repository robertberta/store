
The application already is installed on 35.197.206.198:8080
Swagger 35.197.206.198:8080/swagger-ui.html

To install it yourself:
install mysql with root p@ssw@rd pass (you can change this from resources/application.properties)
in mysql> create database store;
to compile: run mvn clean install
to run the app: java -jar target/store
http://localhost:8080/swagger-ui.html
to run tests: mvn test
sample order search query GET localhost:8080/order/search/findByTime?start=2011-02-25&end=2011-03-28
 - you can also provide time after date , if not provided 0:00:00 is the default

Possible improvements to the project:
price should be changed from double to integer
   - in this case the price would be saved in cents
   - avoids rounding errors that could appear if an order has hundreds of products
store the date of the order as timestamp and replace java.util.Date
add lombok support
move buyer_email to be part of an buyer object
  - in the future more properties will be added to the buyer
add unit test
  - you can check out examples on my other sample project
  - git clone https://robertberta@bitbucket.org/robertberta/microservices.git
add integration test for cases when the service returns 404,500 error codes
disable out of the box provided endpoints products, orders
  -localhost:8080/orders and localhost:8080/products
  -you can use this endpoints for now to delete/view orders and products
add patch to a product
  -this would allow to send only the fields you want to update and leave the rest of the fields unchanged
