package daydev;

/**
 * Created by robert on 07/12/18.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Store {

    public static void main(String[] args) {
        SpringApplication.run(Store.class, args);
        System.out.println("Server up and running");
    }


}
