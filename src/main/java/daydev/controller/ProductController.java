package daydev.controller;

import daydev.exception.ValidationException;
import daydev.model.Product;
import daydev.model.ProductRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by robert on 07/12/18.
 */
@RestController
@Api(value = "store", description = "Operations pertaining to products in Store")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @PostMapping(value = "/product")
    @ApiOperation(value = "Add a new product", response = Product.class)
    public Product addProduct(@RequestBody Product product) throws ValidationException {

        product.validate();


        if (productRepository.findByName(product.getName()) != null) {
            throw new ValidationException("Product with name " + product.getName() + " already exists");
        }

        return productRepository.save(product);
    }

    @GetMapping(value = "/product", produces = "application/json")
    @ApiOperation(value = "View a list of available products", response = Iterable.class)
    public Iterable<Product> readAllProduct() {
        return productRepository.findAll();
    }


    @GetMapping(value = "/product/{id}")
    @ApiOperation(value = "View a product by providing an id", response = Iterable.class)
    public ResponseEntity<Product> readProduct(@PathVariable long id) {

        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            return new ResponseEntity<>(product.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    @PutMapping(value = "/product/{id}", produces = "application/json")
    @ApiOperation(value = "Update a product. If product does not exist it will be added. Please provide all fields for update. For partial updates patch will be added in v2.0", response = Product.class)
    public ResponseEntity<Product> updateProduct(@PathVariable long id, @RequestBody Product product) throws ValidationException {

        product.validate();

        //
        // if product does not exist we save it and return created
        if (!productRepository.existsById(id)) {

            if (productRepository.findByName(product.getName()) != null) {
                throw new ValidationException("Product with name " + product.getName() + " already exists");
            }

            productRepository.save(product);
            return new ResponseEntity<>(product, HttpStatus.CREATED);
        }

        product.setId(id);
        productRepository.save(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }
}
