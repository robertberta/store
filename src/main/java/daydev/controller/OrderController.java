package daydev.controller;

import daydev.exception.ValidationException;
import daydev.model.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by robert on 07/12/18.
 */
@RestController
@Api(value = "store", description = "Operations pertaining to orders in Store")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @PostMapping(value = "/order", produces = "application/json")
    @ApiOperation(value = "Add a new order by providing a list of product names. Products have to be already added.", response = Order.class)
    public Order addOrder(@RequestBody Order order) throws ValidationException {

        for (OrderProduct orderProduct : order.getProducts()) {
            Product product = productRepository.findByName(orderProduct.getName());
            if (product == null) {
                throw new ValidationException("Product " + orderProduct.getName() + " not found");
            }

            orderProduct.setOrder(order);
            orderProduct.setPrice(product.getPrice());

            if (order.getTotalPrice() == null) {
                order.setTotalPrice(orderProduct.getPrice());
            } else {
                order.setTotalPrice(order.getTotalPrice() + orderProduct.getPrice());
            }
            order.setTotalPrice(Precision.round(order.getTotalPrice(), 2));
        }

        order.setId(null);
        return orderRepository.save(order);
    }

    @GetMapping(value = "/order/search/findByTime", produces = "application/json", params = {"start", "end"})
    @ApiOperation(value = "View list of orders by providing start and end date. For start and end if no time is provided 0:00 is assumed.", response = Iterable.class)
    public List<Order> findByTime(@Param(value = "start") String start, @Param(value = "end") String end) {
        return orderRepository.findByTime(start, end);
    }

}
