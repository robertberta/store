package daydev.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by robert on 07/12/18.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
    @Query(value = "SELECT * FROM orders WHERE time > :start AND time < :end", nativeQuery = true)
    List<Order> findByTime(@Param("start") String startTime, @Param("end") String endDate);
}

