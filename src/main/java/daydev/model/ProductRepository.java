package daydev.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by robert on 07/12/18.
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
    Product findByName(@Param("name") String name);
}
