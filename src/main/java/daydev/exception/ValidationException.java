package daydev.exception;

/**
 * Created by robert on 08/12/18.
 */
public class ValidationException extends Exception {
    public ValidationException(String s) {
        super(s);
    }
}
