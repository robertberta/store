package daydev.controller;

/**
 * Created by robert on 08/12/18.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import daydev.model.Product;
import daydev.model.ProductRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;

    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void before() {
        productRepository.deleteAll();
    }

    @After
    public void after() {
        productRepository.deleteAll();
    }


    @Test
    public void addProduct() throws Exception {

        Product product = new Product("sample", 3.99);

        mockMvc.perform(post("/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(product)))
                .andDo(print())
                .andExpect(status().isOk());

        assertEquals(1, productRepository.count());

        Product actualProduct = productRepository.findByName(product.getName());
        actualProduct.setId(null);
        assertEquals(product, actualProduct);
    }

    @Test
    public void updateProduct() throws Exception {

        Product product = new Product("sample", 3.99);
        productRepository.save(product);

        product.setPrice(3.95);

        mockMvc.perform(put("/product/" + productRepository.findAll().iterator().next().getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(product)))
                .andDo(print())
                .andExpect(status().isOk());

        assertEquals(1, productRepository.count());

        Product actualProduct = productRepository.findByName(product.getName());

        actualProduct.setId(null);
        product.setId(null);
        assertEquals(product, actualProduct);
    }

    @Test
    public void getProduct() throws Exception {

        Product product1 = new Product("sample", 3.99);
        Product product2 = new Product("sample2", 2.49);
        productRepository.saveAll(Arrays.asList(product1, product2));


        MvcResult result = this.mockMvc.perform(get("/product"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        List<Product> products = Lists.newArrayList(mapper.readValue(result.getResponse().getContentAsString(), Product[].class));

        product1.setId(null);
        product2.setId(null);
        products.forEach(p -> p.setId(null));

        assertTrue(products.contains(product1));
        assertTrue(products.contains(product2));
        assertTrue(products.size() == 2);
    }


}
