package daydev.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import daydev.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by robert on 08/12/18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderRepository orderRepository;

    private ObjectMapper mapper = new ObjectMapper();

    private List<Product> products = Arrays.asList(new Product("sample1", 1.0), new Product("sample2", 2.0));
    private List<OrderProduct> orderProducts = Arrays.asList(new OrderProduct("sample1"), new OrderProduct("sample2"));
    private Order order = new Order(orderProducts, new Date(1540827062000L), "r@r.com");

    @Before
    public void deleteAll() {
        productRepository.deleteAll();
        orderRepository.deleteAll();
    }

    @After
    public void after() {
        deleteAll();
    }

    @Test
    public void addOrder() throws Exception {

        productRepository.saveAll(products);

        mockMvc.perform(MockMvcRequestBuilders.post("/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(order)))
                .andExpect(status().isOk());

        assertEquals(1, orderRepository.count());

        Order actualOrder = orderRepository.findAll().iterator().next();

        validateOrder(actualOrder);
    }

    @Test
    public void searchOrder() throws Exception {

        productRepository.saveAll(products);
        mockMvc.perform(MockMvcRequestBuilders.post("/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(order)))
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/order/search/findByTime?start=2011-01-01&end=2011-11-02"))
                .andExpect(status().isOk())
                .andReturn();

        List<Order> orders = Lists.newArrayList(mapper.readValue(result.getResponse().getContentAsString(), Order[].class));
        assertEquals(0, orders.size());

        result = mockMvc.perform(MockMvcRequestBuilders.get("/order/search/findByTime?start=2018-10-01&end=2018-11-02"))
                .andExpect(status().isOk())
                .andReturn();

        orders = Lists.newArrayList(mapper.readValue(result.getResponse().getContentAsString(), Order[].class));
        assertEquals(1, orders.size());

        validateOrder(orders.get(0));

    }

    private void validateOrder(Order actualOrder) {
        assertEquals(order.getBuyerEmail(), actualOrder.getBuyerEmail());
        assertEquals(order.getTime(), actualOrder.getTime());
        assertTrue(actualOrder.getTotalPrice() == 3.0);

        orderProducts.get(0).setPrice(1.0);
        orderProducts.get(1).setPrice(2.0);
        actualOrder.getProducts().forEach(p -> {
            p.setId(null);
            p.setOrder(null);
        });

        assertEquals(order.getProducts().size(), actualOrder.getProducts().size());
        assertTrue(order.getProducts().containsAll(actualOrder.getProducts()));

        orderProducts.forEach(p -> p.setPrice(null));
    }


}
